# Cube Info Density

Cross-tab component showing cube info and a density plot.

# Create app and deploy

Clone the repo and build the package then from the package folder run the following.

```
bntools::createApp(tags=c('Visualization Components'), mainCategory = 'Visualization')

```

```
git add -A && git commit -m "add a link to save the image" && git push && git tag -a 1.23 -m "add a link to save the image" && git push --tags
```

```
bntools::deployGitApp('https://bitbucket.org/bnoperator/cubeinfodensity.git', '1.23')
```
 

```
bnshiny::startBNTestShiny('cubeinfodensity')
# see workspace.R for a full example
```

# Change
## 1.23
- add a link to save the image
